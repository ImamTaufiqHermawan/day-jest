const { Post } = require('../models');

module.exports = {
    
    index(req, res) {
        Post.findAll()
        .then(posts => {
            res.status(200).json({
                status: 'success',
                data: {
                    posts
                }
            })    
        })
        .catch(err => {
            res.status(422).json({
                status: 'failed',
                message: err
            })
        })
    },
     create(req, res) {
        const { title, body } = req.body;
        Post.create({
            title,
            body
        })
        .then(post => {
            res.status(201).json({
                status: 'success',
                data: {
                    post
                }
            })
        })
        .catch(err => {
            res.status(422).json({
                status: 'failed',
                message: err
            })
        })
    },
    update(req, res){
        const { title, body } = req.body
        Post.update({ title, body }, 
            {
            where: { id: req.params.id }
            }
        )
        .then(post => {
            res.status(201).json({
            status: 'success',
            message: `Successfuly updated post`,
            result: post
            })
        })
        .catch(err => {
            res.status(422).json({
                status: 'failed',
                message: err
            })
        })
    },
    delete(req, res){
        Post.destroy( 
            {
            where: { id: req.params.id }
            }
        )
        .then(post => {
            res.status(200).json({
            status: 'success',
            message: 'Successfuly deleted post',
            result: post
            })
        })
        .catch(err => {
            res.status(422).json({
                status: 'failed',
                message: err
            })
        })
    }
}
