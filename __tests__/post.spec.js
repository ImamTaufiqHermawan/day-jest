const request = require('supertest');
const app = require('../index')
const { Post } = require('../models');

describe('Posts API Collection', () => {

    beforeAll(() => {
        return Post.destroy({
            truncate: true
        })
    })
    afterAll(() => {
        return Post.destroy({
            truncate: true
        })
    })
    // Jest
    describe('GET /post', () => {
        // Jest
        test('Should successfully get all post', done => {
            // ini dari supertest
            request(app)
            .get('/post')
            .set('Content-Type', 'application/json')
            .then(res => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.status).toEqual('success');
                done();
            })
        })
    })
    describe('GET /post', () => {
        // Jest
        test('Should failed get all post', done => {
            // ini dari supertest
            request(app)
            // If the endpoint is wrong, and then this test will run
            .get('/posts')
            .set('Content-Type', 'application/json')
            .then(res => {
                expect(res.statusCode).toEqual(404);
                done();
            })
        })
    })

    describe('POST /post', () => {
        // Jest
        test('Should successfully create new post', done => {
            // ini dari supertest
            request(app)
            .post('/post')
            .set('Content-Type', 'application/json')
            .send({ title: 'Hello World', body: 'Lorem Ipsum' })
            .then(res => {
                expect(res.statusCode).toEqual(201);
                expect(res.body.status).toEqual('success');
                expect(res.body.data.post)
                .toEqual(
                expect.objectContaining({
                id: expect.any(Number),
                title: expect.any(String),
                body: expect.any(String),
                })
                );
                done();
            })
        })
    })
    describe('PUT /post/:id', () => {
        // Jest
        test('Should successfully update new post', done => {
            // ini dari supertest
            request(app)
            .put('/post/1')
            .set('Content-Type', 'application/json')
            .send({ title: 'Hello World Again', body: 'Lorem Ipsum' })
            .then(res => {
                expect(res.statusCode).toEqual(201);
                expect(res.body.status).toEqual('success');          
                done();
            })
        })
    })
    describe('PUT /post/:id', () => {
        // Jest
        test('Should failed update new post', done => {
            // ini dari supertest
            request(app)
            .put('/post/1')
            .set('Content-Type', 'application/json')
            .send({ title: null, body: null })
            .then(res => {
                expect(res.statusCode).toEqual(422);
                expect(res.body.status).toEqual('failed');
                done();
            })
        })
    })
    describe('DELETE /post/:id', () => {
        // Jest
        test('Should successfully delete new post', done => {
            // ini dari supertest
            request(app)
            .delete('/post/1')
            .set('Content-Type', 'application/json')
            .then(res => {
                expect(res.statusCode).toEqual(200);
                expect(res.body.status).toEqual('success');
                done();
            })
        })
    })
    describe('DELETE /post/:id', () => {
        // Jest
        test('Should failed delete new post', done => {
            // ini dari supertest
            request(app)
            // If it is not a number, then the test will success
            .delete('/post/f')
            .set('Content-Type', 'application/json')
            .then(res => {
                expect(res.statusCode).toEqual(422);
                expect(res.body.status).toEqual('failed');
                done();
            })
        })
    })
})