const express = require('express');
const app = express();
const router = require('./router');
const morgan = require('morgan');

app.use(express.json());
if(process.env.NODE_ENV !== 'test')
    app.use(morgan('dev'));

app.use(router)

module.exports = app;